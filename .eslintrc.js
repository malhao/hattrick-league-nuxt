module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'plugin:vue/recommended',
    'eslint:recommended',
    'standard'
  ],
  plugins: ['vue'],
  // add your custom rules here
  rules: {},
  globals: {
    $nuxt: true
  }
}
